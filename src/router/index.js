import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Login from '@/views/day04/mycom/login.vue'
import Index from '@/views/day04/mycom/index.vue'

//构造函数,通过它实例化路由模块对象
const router = new VueRouter({
  //添加具体的路由配置
  routes: [
    {
      //添加具体的路由配置,每个路由配置都是一个对象,一般包含着name,path,components
      name: 'index', //当前路由名称
      path: '/index', //以后能匹配的路由
      component: Index //当前路由所映射的组件实例,不用写在字符串引号中
    }, {
      name: 'login',
      path: '/login',
      // import:可以指定的模块
      component: () => import('@/views/day04/mycom/login.vue')
    }

  ]


})
export default router