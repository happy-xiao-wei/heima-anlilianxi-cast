import Vue from 'vue'
import VueRouter from 'vue-router'

//安装vue-router
Vue.use(VueRouter)

import Login from '@/views/day04/mycom/login.vue'
import Index from '@/views/day04/mycom/index.vue'


//构造函数,通过它实例化路由模块对象
const router = new VueRouter({
  mode: 'history',
  //添加具体的路由配置
  routes: [
    {
      //添加具体的路由配置,每个路由配置都是一个对象,一般包含着name,path,components
      name: 'index', //当前路由名称
      path: '/index', //以后能匹配的路由
      component: Index,//当前路由所映射的组件实例,不用写在字符串引号中
    },
    {
      //新加一个默认首页
      name: 'default',
      path: '/',
      redirect: { name: 'index' }
    },

    {
      name: 'login',
      path: '/login',
      // import:可以指定的模块
      //import异步加载组件---懒加载
      component: () => import('@/views/day04/mycom/login.vue')
    },
    {
      name: 'school',
      //: 是参数标识
      path: '/school/:id',
      // import: 可以指定的模块
      component: () => import('@/views/day04/mycom/school.vue'),
      redirect: { name: 'children' },//默认显示
      children: [{

        name: 'children',
        path: 'children',
        component: () => import('@/views/day04/mycom/children.vue')
      }]
    },


  ]


})
export default router