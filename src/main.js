import Vue from 'vue'
// 这就是当前项目所渲染的组件
import App from './App.vue'
// import App from './views/day01/01-第一个单文件组件.vue'
// import App from './views/day01/02插值表达式.vue'
// import App from './views/day01/03v-text.vue'
// import App from './views/day01/04 v-HTML.vue'
// import App from './views/day01/05 v-for.vue'
// import App from './views/day01/06 v-model.vue'
// import App from './views/day01/07 v-on.vue'
// import App from './views/day01/test.vue'
// import App from './views/day01/AA账单.vue'
// import App from './views/day02/01-v-bind.vue'
// import App from './views/day02/02-v-bind添加类.vue'
// import App from './views/day02/04局部过滤器创建及使用.vue'
// import App from './views/day02/06钩子mounted(ref函数).vue'
// import App from './views/day02/07自定义指令directives.vue'
// import App from './views/day02/08-计算属性值.vue'
// import App from './views/day02//work.vue'
// import App from './views/day03/02监听器的处使用.vue'
// import App from './views/day03/03axios初使用.vue'
// import App from './views/day03/04axios-post登录.vue'
// import App from './views/day03/05使用封装get.vue'
// import App from './views/day03/06封装post.vue'
// import App from './views/day03/08使用第三方类名实现过渡动画.vue'
// import App from './views/day04/com/father.vue'
// import App from './views/day04/mycom/school.vue'


//引入路由模块
import router from '@/router/myrouter.js'

Vue.config.productionTip = false

new Vue({
  //通过router配置参数注入路由,
  //从而让整个应用都有路由功能
  router, //注入路由
  //在main.js中的一个渲染的组件变是根组件
  render: h => h(App),
}).$mount('#app')












