import axios from 'axios'

//进行全局默认配置
//baseURL:定义请求的基本路径,这个路径后期在发请求的时候会自动的添加到指定
axios.defaults.baseURL = 'http://157.122.54.189:9083/'
//暴露单个
export default axios


// 暴露多个
// export const axios1 = axios.create({
//   baseURL: 'http://157.122.54.189:9083/'
// })

// export const axios2 = axios.create({
//   baseURL: 'http://127.0.0.1:3000/'
// })