//封装所需过滤器

//全局过滤器

export const dateForm = function (data, spe = '-') {
  let year = data.getFullYear();
  //月份从零开始,so + 1
  let month = data.getMonth() + 1;
  let day = data.getDate();
  return `${year}${spe}${month}${spe}${day}-星期`;
}
